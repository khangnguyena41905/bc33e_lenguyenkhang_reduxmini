import React, { Component } from "react";
import { connect } from "react-redux";
import { GIAM, TANG } from "../redux/contant/contant";

class ReduxMini extends Component {
  render() {
    return (
      <div>
        <h1 className="display-4">{this.props.number}</h1>
        <div>
          <button onClick={this.props.giamSoLuong} className="btn btn-warning">
            Giảm
          </button>
          <button
            onClick={this.props.tangSoLuong}
            className="btn btn-success ml-3"
          >
            Tăng
          </button>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    number: state.ReduxMiniReducer.number,
  };
};
let mapDispathToProps = (dispath) => {
  return {
    tangSoLuong: () => {
      dispath({
        type: TANG,
        payload: 1,
      });
    },
    giamSoLuong: () => {
      dispath({
        type: GIAM,
        payload: -1,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispathToProps)(ReduxMini);
