import { GIAM, TANG } from "../contant/contant";

const initialState = { number: 0 };

export const ReduxMiniReducer = (state = initialState, action) => {
  switch (action.type) {
    case TANG: {
      let number = state.number + action.payload;
      state.number = number;
      return { ...state };
    }
    case GIAM: {
      let number = state.number + action.payload;
      state.number = number;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
